# bareboneApp

Install MySQL server. Create 'operators' schema. 
Install node.js + npm (usually they are in one package).

Install sails.js by running this command: 

"sudo npm -g install sails" (Linux)
or
"npm -g install sails" (Windows)

then from the project root folder run:

"sudo npm install" (Linux)
or
"npm install" (Windows)

In order to run project fire this command from project root folder : sails lift

Optional :
1) Install 'nodemon' by running command: "npm install -g nodemon" and run project with command "nodemon app.js"