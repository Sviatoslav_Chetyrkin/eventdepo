module.exports = {
  profile : function (req, res) {
    return VendorService.getUserVendors(req.user.id)
      .then(function(vendors){
        res.view('profile/userProfile', {user_vendors: vendors});
      });
  },

  settings : function (req, res) {
    return res.view('profile/setting');
  },

  content : function (req, res) {
    return VendorService.getUserVendors(req.user.id).then(function (vendors) {
      if(!vendors || vendors.length < 1) return res.view('profile/content', {vendor_id: null});
      
      var vendorId = vendors[0].id;
      return VendorService.getVendorPhotos(vendorId).then(function (photos) {
          return res.view('profile/content', {vendor_id: vendors[0].id, photos: photos});
        });
    })
  },

  update: function (req, res) {
    var form = req.body;
    return UserService.updateUser(form).then(function (result) {
      return res.view('profile/setting');
    })
  }
};

