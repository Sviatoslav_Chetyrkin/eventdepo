module.exports = {

  profile : function(req, res) {
    var vendorId = req.param('vendorId');

    return AlbumService.getVendorAlbums(vendorId).then(function (result) {
      return res.view('vendorProfile', {photo_albums : result});
    })
  },

  create: function (req, res) {
    var form = req.body;
    return VendorService.createVendor(form.userId, form.name)
      .then(function (newVendor) {
        return res.json(newVendor);
      });
  },

  getVendorPhotos: function(req, res){
    var vendorId = req.param('vendorId');
    return VendorService.getVendorPhotos(vendorId);
  }
};
