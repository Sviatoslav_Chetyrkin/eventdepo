module.exports = {

  create: function (req, res) {
    var form = req.body;
    return AlbumService.createPhotoAlbum(form.vendorId, form.title)
      .then(function (newAlbum) {
        return res.json(newAlbum);
      });
  },

  getAlbumImages: function(req, res) {
    var albumId = req.param('albumId');
    return AlbumService.getAlbumImages(albumId);
  }

};
