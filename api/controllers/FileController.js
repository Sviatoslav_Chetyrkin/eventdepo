var fs = require('fs');
var Promise = require('bluebird');
//var wstream = fs.createWriteStream('myOutput.txt');


module.exports = {

  /**
   * `FileController.upload()`
   *
   * Upload file(s) to the server's disk.
   */
  upload: function (req, res) {
    return VendorService.getUserVendors(req.user.id).then(function (vendors) {
      if (!vendors || vendors.length < 1) return false;
      return vendors.pop().id;
    }).then(function (vendor_id) {
      return AlbumService.createDefaultPhotoAlbum(vendor_id);
    }).then(function (album) {
      var storeFolder = __dirname + './../../storeFolder';
      req.file('image')
        .upload({
          dirname: storeFolder
        }, function whenDone(err, uploadedFiles) {
          if (err) return res.serverError(err);
          return Promise.each(uploadedFiles,
            function (file) {
              var names = file.fd.split('/');
              var newFileName = names[names.length - 1];
              return Photo.create({path: newFileName, PhotoAlbum: album.id, title: file.filename});
            }).then(function (files) {
            return res.redirect('/user/content');
          });
        });
    });
  },
  
  getById: function(req, res) {
    var imageId = req.param('imgId');
    return ImageService.getImageById(imageId)
      .then(function(data){
        res.writeHead(200, {'Content-Type': 'image/gif' });
        res.end(data, 'binary');
      });
  }
};
