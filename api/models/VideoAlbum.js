module.exports = {

  identity: 'video_album',

  attributes: {
    title : {
      type : 'string'
    },
    video_album_vendor : {
      model : 'vendor'
    },
    photos : {
      collection : 'video',
      via : 'video_album'
    }
  }

};
