module.exports = {
  schema: true,
  identity: 'Advert',

  attributes: {
    title: {type: 'string'},
    event_date: {type: 'string'},
    profesional: {type: 'integer'},
    cost_min: {type: 'string'},
    cost_max: {type: 'string'},
    location: {type: 'string'},
    description: {
      type: 'text',
      size: 1000
    },
    advert_user : {
      model : 'User'
    }
  }
};
