module.exports = {

  identity: 'Photo',

  attributes: {
    path : {
      type : 'string'
    },
    title : {
      type : 'string'
    },
    description : {
      type: 'text',
      size: 1000
    },
    PhotoAlbum : {
      model : 'PhotoAlbum'
    }
  }

};
