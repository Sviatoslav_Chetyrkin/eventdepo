module.exports = {

  identity: 'PhotoAlbum',

  attributes: {
    title : {
      type : 'string'
    },
    album_vendor : {
      model : 'vendor'
    },
    photos : {
      collection : 'Photo',
      via : 'PhotoAlbum'
    }
  }

};
