module.exports = {

  identity: 'Vendor',

  attributes: {
    name : {
      type : 'string'
    },
    type : {
      type : 'string'
    },
    description : {
      type : 'string'
    },
    upper_price_limit : {
      type : 'integer'
    },
    lower_price_limit : {
      type : 'integer'
    },
    recomendations : {
      collection : 'recomendation',
      via : 'recomendation_vendor'
    },
    photo_albums : {
      collection : 'PhotoAlbum',
      via : 'album_vendor'
    },
    video_albums : {
      collection : 'video_album',
      via : 'video_album_vendor'
    },
    vendor_user : {
      model : 'User'
    }
  }

};
