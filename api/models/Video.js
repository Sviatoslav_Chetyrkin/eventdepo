module.exports = {

  identity: 'video',

  attributes: {
    content : {
      type : 'string'
    },
    video_album : {
      model : 'video_album'
    }
  }

};
