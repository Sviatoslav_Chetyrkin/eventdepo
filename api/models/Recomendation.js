module.exports = {

  identity: 'recomendation',

  attributes: {
    message : {
      type : 'string'
    },
    rate : {
      type : 'integer'
    },
     recomendation_vendor : {
      model : 'vendor'
    }
  }

};
