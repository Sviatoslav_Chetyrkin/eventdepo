module.exports = {
  schema: true,
  identity: 'User',

  attributes: {
    username: {type: 'string'},
    email: {type: 'email', unique: true},
    last_name: {type: 'string'},
    avatar: {type: 'string'},
    sex: {type: 'string'},
    birthday: {type: 'integer'},
    skype: {type: 'string'},
    vk: {type: 'string'},
    fb: {type: 'string'},
    site: {type: 'string'},
    about: {
      type: 'text',
      size: 1000
    },
    passports: {collection: 'passport', via: 'user'},
    user_vendors: {
      collection: 'vendor',
      via: 'vendor_user'
    },
    user_adverts:{
      collection: 'advert',
      via: 'advert_user'

    }
  }
};
