/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {
  //TODO: right now i can't find more elegant way. rewrite in future!!!!
  res.locals.logedIn = !!req.user;
  res.locals.user = req.user;
  // console.log(req.user);
  if(req.user || req.path === '/'){
    return next();
  }
  res.redirect('/login');
};
