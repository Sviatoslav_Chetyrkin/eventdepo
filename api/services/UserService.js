'use strict';
function updateUser(form) {
  return User.update({id: form.userid},
    {
      sex: form.sex,
      birthday: form.birthday,
      vk: form.vk,
      fb: form.fb,
      site: form.site,
      skype: form.skype,
      about: form.about
    }).then(function (result) {
    return result;
  });
}

module.exports = {
  updateUser: updateUser
};
