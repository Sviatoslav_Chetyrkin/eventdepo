var validator = require('validator');
var crypto = require('crypto');

//var User = require('../../models/User');
//var Passport = require('../../models/Passport');

exports.register = function (req, res, next) {

  var email = req.param('email'),
    password = req.param('password'),
    firstName = req.param('firstName'),
    lastName = req.param('lastName'),
    role = req.param('role'), newUser;

  console.log(req.allParams());

  if (!email) {
    req.flash('error', 'Error.Passport.Email.Missing');
    return next(new Error('No email was entered.'));
  }

  if (!password) {
    req.flash('error', 'Error.Passport.Password.Missing');
    return next(new Error('No password was entered.'));
  }

  User.create({
    username: lastName + ' ' + firstName,
    email: email
  }).then(function (user) {
    // Generating accessToken for API authentication
    var token = crypto.randomBytes(48).toString('base64');
    newUser = user;
    return Passport.create({
      protocol: 'local'
      , password: password
      , user: newUser.id
      , accessToken: token
    }).then(function () {

      if(!role || role === '0') {
        return next(null, newUser);
      }

      return VendorService.createVendor(newUser.id, '').then(function () {
        console.log('Vendor is Created');
        return next(null, newUser);
      })
    });
  }).catch(function (err) {

    if (err.code === 'E_VALIDATION' && !newUser) {
      if (err.invalidAttributes.email) {
        req.flash('error', 'Error.Passport.Email.Exists');
      } else {
        req.flash('error', 'Error.Passport.User.Exists');
      }
      return next(err);
    }
    if (err.code === 'E_VALIDATION') {
      req.flash('error', 'Error.Passport.Password.Invalid');
    }
    return newUser.destroy(function (destroyErr) {
      next(destroyErr || err);
    });
  });
};

/**
 * Assign local Passport to user
 *
 * This function can be used to assign a local Passport to a user who doens't
 * have one already. This would be the case if the user registered using a
 * third-party service and therefore never set a password.
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
exports.connect = function (req, res, next) {
  var user = req.user
    , password = req.param('password');

  Passport.findOne({
    protocol: 'local'
    , user: user.id
  }, function (err, passport) {
    if (err) {
      return next(err);
    }

    if (!passport) {
      Passport.create({
        protocol: 'local'
        , password: password
        , user: user.id
      }, function (err, passport) {
        next(err, user);
      });
    }
    else {
      next(null, user);
    }
  });
};

/**
 * Validate a login request
 *
 * Looks up a user using the supplied identifier (email or username) and then
 * attempts to find a local Passport associated with the user. If a Passport is
 * found, its password is checked against the password supplied in the form.
 *
 * @param {Object}   req
 * @param {string}   identifier
 * @param {string}   password
 * @param {Function} next
 */
exports.login = function (req, identifier, password, next) {
  var isEmail = validator.isEmail(identifier)
    , query = {};

  if (isEmail) {
    query.email = identifier;
  }
  else {
    query.username = identifier;
  }

  User.findOne(query, function (err, user) {
    if (err) {
      return next(err);
    }

    if (!user) {
      if (isEmail) {
        req.flash('error', 'Error.Passport.Email.NotFound');
      } else {
        req.flash('error', 'Error.Passport.Username.NotFound');
      }

      return next(null, false);
    }

    Passport.findOne({
      protocol: 'local'
      , user: user.id
    }, function (err, passport) {
      if (passport) {
        passport.validatePassword(password, function (err, res) {
          if (err) {
            return next(err);
          }

          if (!res) {
            req.flash('error', 'Error.Passport.Password.Wrong');
            return next(null, false);
          } else {
            return next(null, user);
          }
        });
      }
      else {
        req.flash('error', 'Error.Passport.Password.NotSet');
        return next(null, false);
      }
    });
  });
};
