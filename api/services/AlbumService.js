var Promise = require('bluebird');

function userAlbums(userId) {
  return User.findById(userId);
}

function getVendorAlbums(vendorId) {
  return Vendor.findOne(vendorId)
    .populate('photo_albums')
    .then(function (vendor) {
      return vendor.photo_albums;
    });
}

function createPhotoAlbum(vendorId, albumTitle) {
  return PhotoAlbum.create({title:albumTitle, album_vendor : vendorId});
}

function createDefaultPhotoAlbum(vendorId) {
  return PhotoAlbum.findOrCreate(
    {
      title: 'index',
      album_vendor: vendorId
    });
}

function getAlbumImages(albumId) {
  return PhotoAlbum.finOne(albumId).populate('photos')
    .then(function(photoAlbum){
      if(!photoAlbum.photos || photoAlbum.photos.length === 0) return [];
      return photoAlbum.photos.map(function(photo){
        return '/img/' + photo.id;
      });
    });
}

module.exports = {
  getUserAlbums : userAlbums,
  createPhotoAlbum : createPhotoAlbum,
  getVendorAlbums : getVendorAlbums,
  createDefaultPhotoAlbum : createDefaultPhotoAlbum,
  getAlbumImages : getAlbumImages
};


