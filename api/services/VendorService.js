'use strict';
var Promise = require('bluebird');

function userVendors(userId) {
  return User.findOne(userId)
    .populate('user_vendors')
    .then(function(user){
      return user.user_vendors;
    });
}

function getVendor(vendorId) {
  return Vendor.findOne(vendorId);
}

function createVendor(userId, vendorName) {
  return Vendor.create({name:vendorName, vendor_user : userId});
}

function getVendorPhotos(vendorId){
  return Vendor.findOne(vendorId).populate('photo_albums').then(function (vendor) {
    return Promise.map(vendor.photo_albums, function(photoAlbum){
      return PhotoAlbum.findOne(photoAlbum.id).populate('photos')
        .then(function(searchedAlbum){
          var results = searchedAlbum.photos.map(function (photo) {
            return '/img/' + photo.id;
          });
          return results;
        });
    }).then(function (res) {
      return res.reduce(function(array1, array2) {
        return array1.concat(array2);
      });
    });
  });
}


module.exports = {
  getVendorById : getVendor,
  getUserVendors : userVendors,
  createVendor : createVendor,
  getVendorPhotos : getVendorPhotos
};
