var Promise = require('bluebird');
var fs = require('fs');

var readFilePromise = Promise.promisify(fs.readFile);

function getImageById(imageId) {
  return Photo.findOne(imageId)
    .then(function(photo){
      var pathToFolder = __dirname + '/../../storeFolder/';
      return readFilePromise(pathToFolder + photo.path);
    });
}

module.exports = {
  getImageById : getImageById
};


